
PER ESEGUIRE L’APPLICAZIONE : 

1- Scaricare Python 2.7 : https://www.python.org/download/releases/2.7/
2- Scaricare Flask : pip install Flask 
3- Scaricare pymysql : pip install PyMySQL
4- Avviare server MySQL 

5- Importare il DB dal file tedxdb.mysql contenuto nella cartella del progetto 

6- Da terminale, posizionarsi nella cartella FlaskApp del progetto 
7- Eseguire il comando :  python app.py 
8- Sul browser, andare in http://localhost:5000/ 