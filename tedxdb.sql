-- MySQL dump 10.13  Distrib 8.0.17, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: tedxb
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `ac`
--

DROP TABLE IF EXISTS `ac`;
/*!50001 DROP VIEW IF EXISTS `ac`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `ac` AS SELECT 
 1 AS `tot`,
 1 AS `evento`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Acquisti`
--

DROP TABLE IF EXISTS `Acquisti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Acquisti` (
  `Spettatore` char(16) NOT NULL,
  `CodiceBiglietto` varchar(10) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Spettatore`,`CodiceBiglietto`,`Evento`),
  UNIQUE KEY `CodiceBiglietto` (`CodiceBiglietto`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `acquisti_ibfk_1` FOREIGN KEY (`Spettatore`) REFERENCES `spettatori` (`CF`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acquisti_ibfk_2` FOREIGN KEY (`CodiceBiglietto`) REFERENCES `biglietti` (`Codice`) ON DELETE CASCADE,
  CONSTRAINT `acquisti_ibfk_3` FOREIGN KEY (`Evento`) REFERENCES `biglietti` (`Evento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Acquisti`
--

LOCK TABLES `Acquisti` WRITE;
/*!40000 ALTER TABLE `Acquisti` DISABLE KEYS */;
INSERT INTO `Acquisti` VALUES ('FLPDMR89C45C469B','5422','Inedito'),('GLGBRG97C45C469B','1000','Inedito'),('FLPDMR89C45C469B','1312','Perspectives'),('LNDVTL97C45C469B','2001','Perspectives'),('MRCSVN97C45C469B','2000','Perspectives'),('FLPPGN97C45C469B','1000','Post Human'),('FRNMNT97C45C469B','1001','Post Human'),('GLGBRG97C45C469B','1002','Post Human'),('LNDVTL97C45C469B','1003','Post Human'),('LNRGSL92C45C469B','6423','Post Human');
/*!40000 ALTER TABLE `Acquisti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Adesioni`
--

DROP TABLE IF EXISTS `Adesioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Adesioni` (
  `Volontario` char(16) NOT NULL,
  `DataRiunione` date NOT NULL,
  `OraRiunione` time NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Volontario`,`DataRiunione`,`OraRiunione`,`Evento`),
  KEY `DataRiunione` (`DataRiunione`),
  KEY `OraRiunione` (`OraRiunione`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `adesioni_ibfk_1` FOREIGN KEY (`Volontario`) REFERENCES `volontari` (`CF`) ON DELETE CASCADE,
  CONSTRAINT `adesioni_ibfk_2` FOREIGN KEY (`DataRiunione`) REFERENCES `riunioni` (`Data`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adesioni_ibfk_3` FOREIGN KEY (`OraRiunione`) REFERENCES `riunioni` (`Ora`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adesioni_ibfk_4` FOREIGN KEY (`Evento`) REFERENCES `riunioni` (`Evento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Adesioni`
--

LOCK TABLES `Adesioni` WRITE;
/*!40000 ALTER TABLE `Adesioni` DISABLE KEYS */;
INSERT INTO `Adesioni` VALUES ('ANNRSS97C45C469B','2019-09-10','15:30:00','post human'),('CLDRTI98C45C469B','2019-09-10','15:30:00','Post Human'),('LUCBNC97C45C469B','2019-09-10','15:30:00','post human'),('ALCVRD97C45C469B','2019-09-28','19:00:00','Post Human'),('IRNFIR98C45C469B','2019-09-28','19:00:00','Post Human'),('ANNRSS97C45C469B','2019-10-17','20:30:00','Perspectives'),('CLDRTI98C45C469B','2019-10-17','20:30:00','Perspectives'),('IRNFIR98C45C469B','2019-10-17','20:30:00','Perspectives'),('SMTABT96C45C469B','2019-10-17','20:30:00','Perspectives');
/*!40000 ALTER TABLE `Adesioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Appartenenze`
--

DROP TABLE IF EXISTS `Appartenenze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Appartenenze` (
  `Volontario` char(16) NOT NULL,
  `Team` enum('DirezioneGenerale','Marketing','Grafica','Comunicazione','Logistica','Regia') NOT NULL,
  `Ruolo` varchar(30) NOT NULL,
  PRIMARY KEY (`Volontario`,`Team`),
  UNIQUE KEY `Volontario` (`Volontario`),
  KEY `Team` (`Team`),
  CONSTRAINT `appartenenze_ibfk_1` FOREIGN KEY (`Volontario`) REFERENCES `volontari` (`CF`) ON DELETE CASCADE,
  CONSTRAINT `appartenenze_ibfk_2` FOREIGN KEY (`Team`) REFERENCES `team` (`Nome`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Appartenenze`
--

LOCK TABLES `Appartenenze` WRITE;
/*!40000 ALTER TABLE `Appartenenze` DISABLE KEYS */;
INSERT INTO `Appartenenze` VALUES ('ALCVRD97C45C469B','Marketing','Fundraiser'),('ANNRSS97C45C469B','DirezioneGenerale','Segretaria'),('BTZLSE97C45C469B','DirezioneGenerale','Supervisore'),('CCLGLL97C45C469B','Marketing','Supervisore'),('CLDRTI98C45C469B','Logistica','Supervisore'),('FLPMRT91C45C469B','Logistica','Autista'),('IRNFIR98C45C469B','Comunicazione','Supervisore'),('LUCBNC97C45C469B','DirezioneGenerale','Assistente'),('MRCGBR96C45C469B','Comunicazione','Admin Instagram'),('NDRBSS80C45C469B','Regia','Supervisore'),('RCCSLV95C45C469B','Grafica','Supervisore'),('SMTABT96C45C469B','Comunicazione','Admin Facebook');
/*!40000 ALTER TABLE `Appartenenze` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BenefitPartner`
--

DROP TABLE IF EXISTS `BenefitPartner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BenefitPartner` (
  `Partner` varchar(30) NOT NULL,
  `CodiceInvito` varchar(10) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Partner`,`CodiceInvito`,`Evento`),
  UNIQUE KEY `CodiceInvito` (`CodiceInvito`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `benefitpartner_ibfk_1` FOREIGN KEY (`Partner`) REFERENCES `partner` (`Nome`) ON DELETE CASCADE,
  CONSTRAINT `benefitpartner_ibfk_2` FOREIGN KEY (`CodiceInvito`) REFERENCES `inviti` (`Codice`) ON DELETE CASCADE,
  CONSTRAINT `benefitpartner_ibfk_3` FOREIGN KEY (`Evento`) REFERENCES `inviti` (`Evento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BenefitPartner`
--

LOCK TABLES `BenefitPartner` WRITE;
/*!40000 ALTER TABLE `BenefitPartner` DISABLE KEYS */;
INSERT INTO `BenefitPartner` VALUES ('24Bottles','1001','Post Human'),('24Bottles','4321','Post Human');
/*!40000 ALTER TABLE `BenefitPartner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BenefitRelatori`
--

DROP TABLE IF EXISTS `BenefitRelatori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BenefitRelatori` (
  `Relatore` char(16) NOT NULL,
  `CodiceInvito` varchar(10) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Relatore`,`CodiceInvito`,`Evento`),
  UNIQUE KEY `CodiceInvito` (`CodiceInvito`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `benefitrelatori_ibfk_1` FOREIGN KEY (`Relatore`) REFERENCES `relatori` (`CF`) ON DELETE CASCADE,
  CONSTRAINT `benefitrelatori_ibfk_2` FOREIGN KEY (`CodiceInvito`) REFERENCES `inviti` (`Codice`) ON DELETE CASCADE,
  CONSTRAINT `benefitrelatori_ibfk_3` FOREIGN KEY (`Evento`) REFERENCES `inviti` (`Evento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BenefitRelatori`
--

LOCK TABLES `BenefitRelatori` WRITE;
/*!40000 ALTER TABLE `BenefitRelatori` DISABLE KEYS */;
/*!40000 ALTER TABLE `BenefitRelatori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BenefitVolontari`
--

DROP TABLE IF EXISTS `BenefitVolontari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BenefitVolontari` (
  `Volontario` char(16) NOT NULL,
  `CodiceInvito` varchar(10) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Volontario`,`CodiceInvito`,`Evento`),
  KEY `CodiceInvito` (`CodiceInvito`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `benefitvolontari_ibfk_1` FOREIGN KEY (`Volontario`) REFERENCES `volontari` (`CF`) ON DELETE CASCADE,
  CONSTRAINT `benefitvolontari_ibfk_2` FOREIGN KEY (`CodiceInvito`) REFERENCES `inviti` (`Codice`) ON DELETE CASCADE,
  CONSTRAINT `benefitvolontari_ibfk_3` FOREIGN KEY (`Evento`) REFERENCES `inviti` (`Evento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BenefitVolontari`
--

LOCK TABLES `BenefitVolontari` WRITE;
/*!40000 ALTER TABLE `BenefitVolontari` DISABLE KEYS */;
INSERT INTO `BenefitVolontari` VALUES ('ANNRSS97C45C469B','1000','Inedito'),('ANNRSS97C45C469B','2000','Perspectives'),('LUCBNC97C45C469B','2001','Perspectives'),('ANNRSS97C45C469B','9201','Post Human');
/*!40000 ALTER TABLE `BenefitVolontari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Biglietti`
--

DROP TABLE IF EXISTS `Biglietti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Biglietti` (
  `Evento` varchar(30) NOT NULL,
  `Codice` varchar(10) NOT NULL,
  PRIMARY KEY (`Codice`,`Evento`),
  UNIQUE KEY `Codice` (`Codice`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `biglietti_ibfk_1` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Biglietti`
--

LOCK TABLES `Biglietti` WRITE;
/*!40000 ALTER TABLE `Biglietti` DISABLE KEYS */;
INSERT INTO `Biglietti` VALUES ('Inedito','1000'),('Inedito','5422'),('Perspectives','1312'),('Perspectives','2000'),('Perspectives','2001'),('Post Human','1000'),('Post Human','1001'),('Post Human','1002'),('Post Human','1003'),('Post Human','6423');
/*!40000 ALTER TABLE `Biglietti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Collaborazioni`
--

DROP TABLE IF EXISTS `Collaborazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Collaborazioni` (
  `Volontario` char(16) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Volontario`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `collaborazioni_ibfk_1` FOREIGN KEY (`Volontario`) REFERENCES `volontari` (`CF`) ON DELETE CASCADE,
  CONSTRAINT `collaborazioni_ibfk_2` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Collaborazioni`
--

LOCK TABLES `Collaborazioni` WRITE;
/*!40000 ALTER TABLE `Collaborazioni` DISABLE KEYS */;
INSERT INTO `Collaborazioni` VALUES ('ALCVRD97C45C469B','Inedito'),('ANNRSS97C45C469B','Inedito'),('BTZLSE97C45C469B','Inedito'),('CCLGLL97C45C469B','Inedito'),('CLDRTI98C45C469B','Inedito'),('IRNFIR98C45C469B','Inedito'),('MRCGBR96C45C469B','Inedito'),('NDRBSS80C45C469B','Inedito'),('RCCSLV95C45C469B','Inedito'),('ANNRSS97C45C469B','Perspectives'),('CLDRTI98C45C469B','Perspectives'),('IRNFIR98C45C469B','Perspectives'),('LUCBNC97C45C469B','Perspectives'),('NDRBSS80C45C469B','Perspectives'),('RCCSLV95C45C469B','Perspectives'),('SMTABT96C45C469B','Perspectives'),('ALCVRD97C45C469B','Post Human'),('ANNRSS97C45C469B','Post Human'),('BTZLSE97C45C469B','Post Human'),('CLDRTI98C45C469B','Post Human'),('FLPMRT91C45C469B','Post Human'),('IRNFIR98C45C469B','Post Human'),('LUCBNC97C45C469B','Post Human'),('RCCSLV95C45C469B','Post Human');
/*!40000 ALTER TABLE `Collaborazioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Eventi`
--

DROP TABLE IF EXISTS `Eventi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Eventi` (
  `Titolo` varchar(30) NOT NULL,
  `Tipologia` enum('Standard','Youth','Woman','Salon') NOT NULL DEFAULT 'Standard',
  `Data` date NOT NULL,
  `NumeroLicenza` varchar(10) NOT NULL,
  `SitoWeb` varchar(30) DEFAULT NULL,
  `Programma` text,
  PRIMARY KEY (`Titolo`),
  UNIQUE KEY `Data` (`Data`),
  UNIQUE KEY `NumeroLicenza` (`NumeroLicenza`),
  UNIQUE KEY `Data_2` (`Data`),
  UNIQUE KEY `Titolo` (`Titolo`),
  UNIQUE KEY `NumeroLicenza_2` (`NumeroLicenza`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Eventi`
--

LOCK TABLES `Eventi` WRITE;
/*!40000 ALTER TABLE `Eventi` DISABLE KEYS */;
INSERT INTO `Eventi` VALUES ('Inedito','Standard','2020-01-25','023952','www.tedxbolognainedito.it','14:30 ENTRATA E ACCREDITO //  15:00 INIZIO //  15:15 TALK 1 //  15:30 TALK 2 //  16:00 COFFE-BREAK //  18:00 RINGRAZIAMENTI'),('Perspectives','Woman','2019-12-07','139301','www.tedxbw-perspectives.it',NULL),('Post Human','Standard','2019-09-21','023958','www.tedxbolognaposthuman.it',NULL);
/*!40000 ALTER TABLE `Eventi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Interventi`
--

DROP TABLE IF EXISTS `Interventi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Interventi` (
  `Relatore` char(16) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  `TitoloTalk` varchar(30) DEFAULT NULL,
  `DurataTalk` int(11) NOT NULL,
  PRIMARY KEY (`Relatore`,`Evento`),
  UNIQUE KEY `Relatore` (`Relatore`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `interventi_ibfk_1` FOREIGN KEY (`Relatore`) REFERENCES `relatori` (`CF`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `interventi_ibfk_2` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Interventi`
--

LOCK TABLES `Interventi` WRITE;
/*!40000 ALTER TABLE `Interventi` DISABLE KEYS */;
INSERT INTO `Interventi` VALUES ('CLSGVN07C45C469B','Perspectives','Arte di argomentare',12),('DNNCHN90C45C469B','Inedito','Robots today',15),('GLCGRD89C45C469B','Perspectives','Il suono',14),('GLCGRD89C45C469B','Post Human','Musica e AI',12),('MBRFRN70C45C469B','Perspectives','Creare un futuro inedito',17),('MCLFRR51C45C469B','Post Human','Intelligenza artificiale',14),('SSNTRK89C45C469B','Post Human','Bitnation',13);
/*!40000 ALTER TABLE `Interventi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Inviti`
--

DROP TABLE IF EXISTS `Inviti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Inviti` (
  `Evento` varchar(30) NOT NULL,
  `Codice` varchar(10) NOT NULL,
  PRIMARY KEY (`Codice`,`Evento`),
  UNIQUE KEY `Codice` (`Codice`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `inviti_ibfk_1` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inviti`
--

LOCK TABLES `Inviti` WRITE;
/*!40000 ALTER TABLE `Inviti` DISABLE KEYS */;
INSERT INTO `Inviti` VALUES ('Inedito','1000'),('Perspectives','2000'),('Perspectives','2001'),('Post Human','1000'),('Post Human','1001'),('Post Human','1002'),('Post Human','1003'),('Post Human','4321'),('Post Human','9201');
/*!40000 ALTER TABLE `Inviti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Location`
--

DROP TABLE IF EXISTS `Location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Location` (
  `Nome` varchar(20) NOT NULL,
  `Indirizzo` varchar(30) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Capienza` int(11) NOT NULL,
  `PrezzoBiglietto` float NOT NULL,
  PRIMARY KEY (`Nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Location`
--

LOCK TABLES `Location` WRITE;
/*!40000 ALTER TABLE `Location` DISABLE KEYS */;
INSERT INTO `Location` VALUES ('MAST','via Speranza 42 (BO)','0516872354',450,50),('Opificio Golinelli','via Costa 14 (BO)','0510923120',400,35),('Teatro Comunale','Via vivaldi 14 Bologna','0516834546',800,35);
/*!40000 ALTER TABLE `Location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `om`
--

DROP TABLE IF EXISTS `om`;
/*!50001 DROP VIEW IF EXISTS `om`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `om` AS SELECT 
 1 AS `tot`,
 1 AS `evento`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Omaggi`
--

DROP TABLE IF EXISTS `Omaggi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Omaggi` (
  `Spettatore` char(16) NOT NULL,
  `CodiceInvito` varchar(10) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Spettatore`,`CodiceInvito`,`Evento`),
  UNIQUE KEY `CodiceInvito` (`CodiceInvito`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `omaggi_ibfk_1` FOREIGN KEY (`Spettatore`) REFERENCES `spettatori` (`CF`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `omaggi_ibfk_2` FOREIGN KEY (`CodiceInvito`) REFERENCES `inviti` (`Codice`) ON DELETE CASCADE,
  CONSTRAINT `omaggi_ibfk_3` FOREIGN KEY (`Evento`) REFERENCES `inviti` (`Evento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Omaggi`
--

LOCK TABLES `Omaggi` WRITE;
/*!40000 ALTER TABLE `Omaggi` DISABLE KEYS */;
INSERT INTO `Omaggi` VALUES ('FLPPGN97C45C469B','1001','Inedito'),('MRCSVN97C45C469B','1000','Inedito'),('FLPPGN97C45C469B','2000','Perspectives'),('FRNMNT97C45C469B','2001','Perspectives'),('FLPDMR89C45C469B','9201','Post Human'),('LNRGSL92C45C469B','4321','Post Human'),('MRCSVN97C45C469B','1001','Post Human'),('NRCSLV97C45C469B','1002','Post Human'),('SRMRNI97C45C469B','1003','Post Human');
/*!40000 ALTER TABLE `Omaggi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partner` (
  `Nome` varchar(30) NOT NULL,
  `Referente` varchar(30) DEFAULT NULL,
  `Email` varchar(20) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `NumeroEventi` int(11) NOT NULL DEFAULT '0',
  `Categoria` enum('Main','Silver','Supporter') NOT NULL DEFAULT 'Supporter',
  PRIMARY KEY (`Nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES ('24Bottles','Luca Grandi','tfbot@info.it','0513459876',3,'Silver'),('Coop','Cristina Balboni','coop@office.it','05134526754',2,'Supporter'),('Hera','Claudia Velli','hera@marketing.it','0598372991',2,'Supporter'),('Wind','Carlo Tinti','windoffice@gmail.com','3329876543',1,'Supporter');
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Relatori`
--

DROP TABLE IF EXISTS `Relatori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Relatori` (
  `CF` char(16) NOT NULL,
  `Nome` varchar(20) DEFAULT NULL,
  `Cognome` varchar(20) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Email` varchar(20) DEFAULT NULL,
  `Provenienza` varchar(20) DEFAULT NULL,
  `Professione` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Relatori`
--

LOCK TABLES `Relatori` WRITE;
/*!40000 ALTER TABLE `Relatori` DISABLE KEYS */;
INSERT INTO `Relatori` VALUES ('CLSGVN07C45C469B','Celeste','Govoni','3895076887','govcel@gmail.com','Umbria','Docente'),('DNNCHN90C45C469B','Dan','Chen','3895026887','danchen@mail.com','Los Angeles','Inventore'),('GLCGRD89C45C469B','Gianluca','Giordano','3493131341','giordanog@outlook.it','Modena','Musicista'),('MBRFRN70C45C469B','Ambra','Fiorini','3343162714','ambrafio@libero.it','Ferrara','Attrice'),('MCLFRR51C45C469B','Michael','Ferri','3351352151','ferrim@alice.com','Mantova','Scultore'),('SSNTRK89C45C469B','Susanne','Tarkowski','3256969876','stkwsi@mail.com','Olanda','Imprenditrice');
/*!40000 ALTER TABLE `Relatori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Riunioni`
--

DROP TABLE IF EXISTS `Riunioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Riunioni` (
  `Evento` varchar(30) NOT NULL,
  `Data` date NOT NULL,
  `Ora` time NOT NULL,
  `Luogo` varchar(30) DEFAULT NULL,
  `ODG` text,
  PRIMARY KEY (`Data`,`Ora`,`Evento`),
  UNIQUE KEY `Evento` (`Evento`,`Data`,`Ora`),
  KEY `DataRiunione` (`Data`),
  KEY `OraRiunione` (`Ora`),
  CONSTRAINT `riunioni_ibfk_1` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Riunioni`
--

LOCK TABLES `Riunioni` WRITE;
/*!40000 ALTER TABLE `Riunioni` DISABLE KEYS */;
INSERT INTO `Riunioni` VALUES ('Post Human','2019-09-10','15:30:00','Sede Associazione',NULL),('Post Human','2019-09-28','19:00:00','Bologna-via Lunga','- accordi con location \n- alloggio relatori\n- problema entrata anticipata'),('Perspectives','2019-10-17','20:30:00','Casa Sara','- nuovo programma');
/*!40000 ALTER TABLE `Riunioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sedi`
--

DROP TABLE IF EXISTS `Sedi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Sedi` (
  `Location` varchar(20) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  PRIMARY KEY (`Location`,`Evento`),
  KEY `Evento` (`Evento`),
  CONSTRAINT `sedi_ibfk_1` FOREIGN KEY (`Location`) REFERENCES `location` (`Nome`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sedi_ibfk_2` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sedi`
--

LOCK TABLES `Sedi` WRITE;
/*!40000 ALTER TABLE `Sedi` DISABLE KEYS */;
INSERT INTO `Sedi` VALUES ('Teatro Comunale','Inedito'),('MAST','Perspectives'),('Teatro Comunale','Post Human');
/*!40000 ALTER TABLE `Sedi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sovvenzioni`
--

DROP TABLE IF EXISTS `Sovvenzioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Sovvenzioni` (
  `Sponsorizzazione` varchar(10) NOT NULL,
  `Evento` varchar(30) NOT NULL,
  `Partner` varchar(30) NOT NULL,
  PRIMARY KEY (`Sponsorizzazione`,`Evento`,`Partner`),
  UNIQUE KEY `Sponsorizzazione` (`Sponsorizzazione`,`Evento`,`Partner`),
  KEY `Evento` (`Evento`),
  KEY `Partner` (`Partner`),
  CONSTRAINT `sovvenzioni_ibfk_1` FOREIGN KEY (`Sponsorizzazione`) REFERENCES `sponsorizzazioni` (`Codice`) ON DELETE CASCADE,
  CONSTRAINT `sovvenzioni_ibfk_2` FOREIGN KEY (`Evento`) REFERENCES `sponsorizzazioni` (`Evento`) ON DELETE CASCADE,
  CONSTRAINT `sovvenzioni_ibfk_3` FOREIGN KEY (`Partner`) REFERENCES `partner` (`Nome`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sovvenzioni`
--

LOCK TABLES `Sovvenzioni` WRITE;
/*!40000 ALTER TABLE `Sovvenzioni` DISABLE KEYS */;
INSERT INTO `Sovvenzioni` VALUES ('1236','Inedito','24Bottles'),('1820','Inedito','Coop'),('8012','Inedito','Hera'),('2810','Perspectives','Hera'),('3901','Perspectives','24Bottles'),('1234','Post Human','24Bottles');
/*!40000 ALTER TABLE `Sovvenzioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Spese`
--

DROP TABLE IF EXISTS `Spese`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Spese` (
  `Evento` varchar(30) NOT NULL,
  `Codice` varchar(10) NOT NULL,
  `Importo` float NOT NULL,
  `Descrizione` tinytext,
  PRIMARY KEY (`Evento`,`Codice`),
  UNIQUE KEY `Evento` (`Evento`,`Codice`),
  CONSTRAINT `spese_ibfk_1` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Spese`
--

LOCK TABLES `Spese` WRITE;
/*!40000 ALTER TABLE `Spese` DISABLE KEYS */;
INSERT INTO `Spese` VALUES ('Inedito','3908',2000,'Location'),('Perspectives','1391',780,'Volo aereo'),('Perspectives','90830',2400,'Location'),('Post Human','1939',250,'Proiettore'),('Post Human','2911',3000,'Location'),('Post Human','3901',1500,'Hotel ');
/*!40000 ALTER TABLE `Spese` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Spettatori`
--

DROP TABLE IF EXISTS `Spettatori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Spettatori` (
  `CF` char(16) NOT NULL,
  `Nome` varchar(20) DEFAULT NULL,
  `Cognome` varchar(20) DEFAULT NULL,
  `DataNascita` date DEFAULT NULL,
  `Sesso` enum('M','F') DEFAULT NULL,
  `Residenza` varchar(20) DEFAULT NULL,
  `Occupazione` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Spettatori`
--

LOCK TABLES `Spettatori` WRITE;
/*!40000 ALTER TABLE `Spettatori` DISABLE KEYS */;
INSERT INTO `Spettatori` VALUES ('FLPDMR89C45C469B','Filippo','DeMaria','1989-07-13','M','Modena','Operaio'),('FLPPGN97C45C469B','Filippo','Paganelli','1992-02-15','M','Santarcangelo di Ro','Studente'),('FRNMNT97C45C469B','Francesco','Montelli','1997-08-01','M','Ravenna','Studente'),('GLGBRG97C45C469B','Giulia','Brugnatti','1997-02-15','F','Ravenna','Studente'),('GVNSNC70C45C469B','Giovanni','Soncin','1970-11-14','M','Cento (FE)','Impiegato'),('LNDVTL97C45C469B','Linda','Vitali','1994-02-15','F','Cervia','Studente'),('LNRGSL92C45C469B','Leonardo','Guselli','1992-02-13','M','Cento (FE)','Ingegnere'),('MRCSVN97C45C469B','Marco','Savini','1992-03-12','M','Senigallia','Studente'),('NRCSLV97C45C469B','Enrico','Salvi','1992-03-12','M','Cesena','Studente'),('SRMRNI97C45C469B','Sara','Morini','1997-12-01','F','Ravenna','Studente');
/*!40000 ALTER TABLE `Spettatori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sponsorizzazioni`
--

DROP TABLE IF EXISTS `Sponsorizzazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Sponsorizzazioni` (
  `Evento` varchar(30) NOT NULL,
  `Codice` varchar(10) NOT NULL,
  `Tipo` enum('Inkind','Monetaria') NOT NULL DEFAULT 'Monetaria',
  `Importo` float NOT NULL,
  `Descrizione` text,
  PRIMARY KEY (`Evento`,`Codice`),
  UNIQUE KEY `Evento_2` (`Evento`,`Codice`),
  KEY `Evento` (`Evento`),
  KEY `Sponsorizzazione` (`Codice`),
  CONSTRAINT `sponsorizzazioni_ibfk_1` FOREIGN KEY (`Evento`) REFERENCES `eventi` (`Titolo`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sponsorizzazioni`
--

LOCK TABLES `Sponsorizzazioni` WRITE;
/*!40000 ALTER TABLE `Sponsorizzazioni` DISABLE KEYS */;
INSERT INTO `Sponsorizzazioni` VALUES ('Inedito','1236','Monetaria',1000,NULL),('Inedito','1820','Monetaria',700,''),('Inedito','8012','Monetaria',1500,''),('Perspectives','2810','Monetaria',860,''),('Perspectives','3901','Inkind',500,'Bottiglie per relatori e partner'),('Post Human','1234','Monetaria',2000,'');
/*!40000 ALTER TABLE `Sponsorizzazioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Team`
--

DROP TABLE IF EXISTS `Team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Team` (
  `Nome` enum('DirezioneGenerale','Marketing','Grafica','Comunicazione','Logistica','Regia') NOT NULL,
  `Supervisore` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  PRIMARY KEY (`Nome`),
  UNIQUE KEY `Nome` (`Nome`),
  UNIQUE KEY `Supervisore` (`Supervisore`),
  UNIQUE KEY `Supervisore_2` (`Supervisore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Team`
--

LOCK TABLES `Team` WRITE;
/*!40000 ALTER TABLE `Team` DISABLE KEYS */;
INSERT INTO `Team` VALUES ('DirezioneGenerale','Elisa Betz','admin'),('Marketing','Cecilia Galli','admin'),('Grafica','Riccardo Salvi','admin'),('Comunicazione','Irene Fiore','admin'),('Logistica','Claudio Orti','admin'),('Regia','Andrea Bassi','admin');
/*!40000 ALTER TABLE `Team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Volontari`
--

DROP TABLE IF EXISTS `Volontari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Volontari` (
  `CF` char(16) NOT NULL,
  `Nome` varchar(20) DEFAULT NULL,
  `Cognome` varchar(20) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Skype` varchar(15) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`CF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Volontari`
--

LOCK TABLES `Volontari` WRITE;
/*!40000 ALTER TABLE `Volontari` DISABLE KEYS */;
INSERT INTO `Volontari` VALUES ('ALCVRD97C45C469B','Alice','Verdi','3458769034','verali','alicev@libero.it'),('ANNRSS97C45C469B','Anna','Rossi','3703384749','rossianna','annarossi@tedxbologna.com'),('BTZLSE97C45C469B','Elisa','Betz','3703384947','betzelisa','elisabetz@tedxbologna.com'),('CCLGLL97C45C469B','Cecilia','Galli','3895076887','cecigal','ceciliagalli@tedxbologna.com'),('CLDRTI98C45C469B','Claudio','Orti','3383131341','claudiort','claudiorti@outlook.it'),('FLPMRT91C45C469B','Filippo','Martelli','3457889765','martellifil','filippomartelli@gmail.com'),('IRNFIR98C45C469B','Irene','Fiore','3426557894','fiore','irenefiore@tedxbologna.com'),('LUCBNC97C45C469B','Luca','Bianchi','3385469071','bianchiluc','luca@gmail.com'),('MRCGBR96C45C469B','Marco','Gabrieli','3296331879','gabm','marcogabrieli@tedxbologna.com'),('NDRBSS80C45C469B','Andrea','Bassi','3343162714','bassian','andreabassi@tedxbologna.com'),('RCCSLV95C45C469B','Riccardo','Salvi','3293134352','riccardosalvi','riccardosalvi@tedxbologna.com'),('SMTABT96C45C469B','Samanta','Abati','3496784567','semabati','abatisam@gmail.com');
/*!40000 ALTER TABLE `Volontari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `ac`
--

/*!50001 DROP VIEW IF EXISTS `ac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ac` AS select count(`acquisti`.`Spettatore`) AS `tot`,`acquisti`.`Evento` AS `evento` from `acquisti` group by `acquisti`.`Evento` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `om`
--

/*!50001 DROP VIEW IF EXISTS `om`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `om` AS select count(`omaggi`.`Spettatore`) AS `tot`,`omaggi`.`Evento` AS `evento` from `omaggi` group by `omaggi`.`Evento` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-09 11:02:02
