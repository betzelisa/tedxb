# python app.py

from flask import Flask, render_template, request, url_for, redirect, session
import pymysql


app = Flask(__name__)
app.secret_key = "abcdefg"


def connectdb():
    return pymysql.connect(host='localhost', user='root', password='4April1984', db='tedxb', charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)


def getvolunteers():
    volontari = []
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute(
                "select v.*, a.ruolo as ruolo from volontari v, appartenenze a where a.volontario = v.cf and a.team = (%s)",
                session["team"])
            rows = cursor.fetchall()

            for row in rows:
                volontario = {"cf": (row["CF"]), "nome": (row["Nome"]), "cognome": (row["Cognome"]),
                              "telefono": (row["Telefono"]), "skype": (row["Skype"]), "email": (row["Email"]),
                              "ruolo": (row["ruolo"])}
                volontari.append(volontario)

    except Exception as e:
        print(e)

    return volontari


def getevents():
    eventi = []
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute("select e.*, s.location as location from eventi e, sedi s where s.evento=e.titolo")
            rows = cursor.fetchall()

            for row in rows:
                evento = {"titolo": (row["Titolo"]), "tipologia": (row["Tipologia"]), "data": (row["Data"]),
                          "location": (row["location"])}
                eventi.append(evento)

    except Exception as e:
        print(e)

    return eventi


def getmeetings():
    riunioni = []
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute("select * from riunioni where data >= curdate()")
            rows = cursor.fetchall()

            n = 0
            for row in rows:
                n = n + 1
                riunione = {"evento": (row["Evento"]), "data": (row["Data"]), "ora": (row["Ora"]),
                            "luogo": (row["Luogo"]), "ID": str(n)}
                riunioni.append(riunione)

    except Exception as e:
        print(e)

    return riunioni


def getaudience():
    spettatori = []
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute(
                "select (IFNULL(tab.actot,0) + IFNULL(tab.omtot,0)) as numspet, tab.evento from ((select ac.tot as actot, om.tot as omtot, ac.evento from om right join ac on ac.evento = om.evento union select ac.tot as actot, om.tot as amtot, ac.evento from om left join ac on ac.evento = om.evento)) as tab")
            rows = cursor.fetchall()

            for row in rows:
                spettatori.append({"evento": (row["evento"]), "numspet": (row["numspet"])})

    except Exception as e:
        print(e)

    return spettatori


def getcharges():
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute("select * from spese")
            rows = cursor.fetchall()
            spese = []

            for row in rows:
                spesa = {"evento": (row["Evento"]), "codice": (row["Codice"]), "importo": (row["Importo"]),
                         "descrizione": (row["Descrizione"])}
                spese.append(spesa)

    except Exception as e:
        print(e)

    return spese


def getavgcharge():
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute( "SELECT E.Tipologia, AVG(spesatotale.importo) as Media FROM Eventi E, Spese S, (select sum(importo) as importo, evento from spese group by evento) as spesatotale WHERE E.Titolo = S.Evento  AND E.Titolo = spesatotale.evento GROUP BY E.Tipologia")
            rows = cursor.fetchall()
            avgs = []
            for row in rows:
                avg = {"tipologia": (row["Tipologia"]), "media": (row["Media"])}
                avgs.append(avg)

    except Exception as e:
        print(e)

    return avgs


def getfstat():
    eventi = getevents()
    bilanci = []
    try:
        connection = connectdb()
        with connection.cursor() as cursor:
            for evento in eventi:
                titoloev = evento["titolo"]
                cursor.execute(
                    "SELECT(	SELECT ( SELECT ( SELECT L.PrezzoBiglietto FROM Sedi Sd JOIN Location L ON(Sd.Location = L.Nome) WHERE  Sd.Evento = (%s)) * COUNT(CodiceBiglietto) FROM Acquisti WHERE  Evento = (%s)) + SUM(Importo) FROM Sponsorizzazioni WHERE Evento = (%s) AND Tipo = 'Monetaria') - SUM(Importo) as bilancio FROM Spese WHERE Evento = (%s)",
                    (titoloev, titoloev, titoloev, titoloev))
                row = cursor.fetchone()
                bilancio = {"evento": titoloev, "bilancio": (row["bilancio"])}
                bilanci.append(bilancio)

    except Exception as e:
        print(e)

    return bilanci


def getspeakerdetails(evento):
    try:
        connection = connectdb()
        with connection.cursor() as cursor:
            sql = "select r.provenienza, r.cf, r.nome, r.cognome, i.titolotalk, i.duratatalk from interventi i, relatori r where i.relatore = r.cf and i.evento = (%s)"
            cursor.execute(sql, evento)
            rows = cursor.fetchall()
            dettaglio = []
            for row in rows:
                dettaglio.append(
                    {"evento": evento, "nome": (row["nome"]), "cognome": (row["cognome"]), "talk": (row["titolotalk"]),
                     "durata": (row["duratatalk"]), "cf": (row["cf"]), "provenienza": (row["provenienza"])})

    except Exception as e:
        print(e)

    return dettaglio


def checkinvite(invitante, riferimento, evento):
    try:
        if invitante == "partner":
            connection = connectdb()
            with connection.cursor() as cursor:
                # controllo se il partner sponsorizza l'evento
                sql = "select * from sovvenzioni where partner = (%s) and evento = (%s)"
                cursor.execute(sql, (riferimento, evento))
                if cursor.fetchone():
                    # controllo in base alla categoria del partner che abbia ancora inviti disponibili
                    sql = "select p.categoria, count(b.codiceinvito) as num from benefitpartner b, partner p where partner = (%s) and evento = (%s) and b.partner=p.nome"
                    cursor.execute(sql, (riferimento, evento))
                    result = cursor.fetchone()
                    if result:
                        if result["categoria"] == "Main" and result["num"] < 5:
                            return "p"
                        elif result["categoria"] == "Silver" and result["num"] < 3:
                            return "p"
                        elif result["num"] == 0:
                            return "p"
                        else:
                            return "Il partner ha terminato gli inviti disponibili per questo evento."
                    else:
                        return "p"
                else:
                    return "Il partner non ha sponsorizzato questo evento."

        elif invitante == "volontario":
            print(invitante)
            connection = connectdb()
            with connection.cursor() as cursor:
                # controllo se il volontario collabora all'evento
                sql = "select * from collaborazioni where volontario = (%s) and evento = (%s)"
                cursor.execute(sql, (riferimento, evento))
                if cursor.fetchone():
                    # controllo che il volontario non abbia gia utilizzato un invito per lo stesso evento
                    sql = "select * from benefitvolontari where volontario = (%s) and evento = (%s)"
                    cursor.execute(sql, (riferimento, evento))
                    if cursor.fetchone():
                        return "Il volontario ha terminato gli inviti disponibili per questo evento."
                    else:
                        return "v"
                else:
                    return "Il volontario non ha collaborato a questo evento."

        elif invitante == "relatore":
            print(invitante)
            connection = connectdb()
            with connection.cursor() as cursor:
                # controllo se il relatore partecipa all'evento
                sql = "select * from interventi where relatore = (%s) and evento = (%s)"
                cursor.execute(sql, (riferimento, evento))
                if cursor.fetchone():
                    # controllo che il relatore non abbia gia utilizzato un invito per lo stesso evento
                    sql = "select * from benefitrelatori where relatore = (%s) and evento = (%s)"
                    cursor.execute(sql, (riferimento, evento))
                    if cursor.fetchone():
                        return "Il relatore ha terminato gli inviti disponibili per questo evento."
                    else:
                        return "r"
                else:
                    return "Il relatore non partecipa a questo evento."

    except:
        return "Errore"

    else:
        return "Errore"


def checkeventcapacity(evento):
    try:
        connection = connectdb()
        with connection.cursor() as cursor:
            cursor.execute((
                "select l.capienza - (IFNULL(tab.actot,0) + IFNULL(tab.omtot,0)) as postiliberi from (select ac.tot as actot, om.tot as omtot, ac.evento from om right join ac on ac.evento = om.evento union select ac.tot as actot, om.tot as amtot, ac.evento from om left join ac on ac.evento = om.evento) as tab, location l, sedi s where tab.evento = (%s) and tab.evento = s.evento and s.location = l.nome"),
                evento)
            row = cursor.fetchone()

    except Exception as e:
        print(e)

    return row["postiliberi"]


def getpartnersdetail(evento):
    try:
        connection = connectdb()
        with connection.cursor() as cursor:
            sql = "select sv.partner, sp.tipo, sp.importo, sp.descrizione from sovvenzioni sv, sponsorizzazioni sp where sv.sponsorizzazione = sp.codice and sv.evento = sp.evento and sv.evento=(%s)"
            cursor.execute(sql, evento)
            rows = cursor.fetchall()
            dettaglio = []
            for row in rows:
                dettaglio.append({"evento": evento, "partner": (row["partner"]), "importo": (row["importo"]),
                                  "descrizione": (row["descrizione"]), "tipo": (row["tipo"])})

    except Exception as e:
        print(e)

    return dettaglio


def getchargesdetail(evento):
    try:
        connection = connectdb()
        with connection.cursor() as cursor:
            sql = "select * from spese where evento=(%s)"
            cursor.execute(sql, evento)
            rows = cursor.fetchall()
            dettaglio = []
            for row in rows:
                dettaglio.append({"evento": evento, "importo": (row["Importo"]), "descrizione": (row["Descrizione"])})

    except Exception as e:
        print(e)

    return dettaglio


def getsponsorshipdetail():
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute(
                "select evento, sum(importo) as importo from sponsorizzazioni where tipo = 'Monetaria' group by evento")
            rows = cursor.fetchall()
            spons = []
            for row in rows:
                s = {"evento": (row["evento"]), "importo": (row["importo"])}
                spons.append(s)

    except Exception as e:
        print(e)

    return spons


def getlocation():
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            cursor.execute(
                "select * from location")
            rows = cursor.fetchall()
            location = []

            for row in rows:
                location.append({"nome": (row["Nome"]), "indirizzo": (row["Indirizzo"]), "telefono": (row["Telefono"]),
                                 "capienza": (row["Capienza"]), "prezzobiglietto": (row["PrezzoBiglietto"])})

    except Exception as e:
        print(e)

    return location


def getduration():
    durate = []
    try:
        connection = connectdb()
        eventi = getevents()
        for evento in eventi:
            with connection.cursor() as cursor:
                cursor.execute("SELECT SUM(DurataTalk) AS duratatotale FROM Interventi WHERE Evento = (%s)",
                               evento["titolo"])
                row = cursor.fetchone()
                durate.append({"evento": evento["titolo"], "durata": row["duratatotale"]})

    except Exception as e:
        print(e)

    return durate


# -------------------------------------------------------------PAGINE
@app.route("/", methods=["POST", "GET"])  # LOGIN
def main():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:

            if request.method == "POST":
                att_team = request.form['team']
                att_password = request.form['password']

                # viene controllato se esiste il team e se la password e corretta
                sql1 = "select nome from team where nome = (%s) and password = (%s)"
                cursor.execute(sql1, (att_team, att_password))
                team = cursor.fetchone()

                # se e stato inserito qualche dato non corretto, viene mostrato un messaggio di errore
                if team == None:
                    error = "Dati inseriti non validi!"
                    return render_template('login.html', error=error)

                # se i dati sono corretti, si apre la home page del team corrispondente
                else:
                    #operazioni batch
                    cursor.execute("UPDATE Partner SET Categoria = 'Silver' WHERE NumeroEventi >= 3 AND Categoria = 'Supporter'")
                    cursor.execute("UPDATE Partner SET Categoria = 'Main' WHERE NumeroEventi >= 5 AND Categoria = 'Silver'")
                    cursor.execute("DELETE Riunioni.* FROM Riunioni WHERE Data < CURDATE()")
                    connection.commit()

                    team = att_team.lower()
                    session['team'] = team
                    if team == 'direzionegenerale':
                        return redirect(url_for("direzione"))
                    elif team == 'marketing':
                        return redirect(url_for("marketing"))
                    elif team == 'comunicazione':
                        return redirect(url_for("comunicazione"))
                    elif team == 'logistica':
                        return redirect(url_for("logistica"))
                    elif team == 'grafica':
                        return redirect(url_for("grafica"))
                    elif team == 'regia':
                        return redirect(url_for("regia"))
    finally:
        render_template("login.html")

    return render_template("login.html")


@app.route("/direzione-generale/", methods=["GET", "POST"])
def direzione():
    volontari = getvolunteers()
    eventi = getevents()
    riunioni = getmeetings()
    spettatori = getaudience()

    dettagli = []
    for evento in eventi:
        dettaglio = getspeakerdetails(evento["titolo"])
        dettagli.append(dettaglio)

    return render_template('home-direzione.html', volontari=volontari, eventi=eventi, riunioni=riunioni,
                           spettatori=spettatori, dettagli=dettagli)


@app.route("/marketing/")
def marketing():
    volontari = getvolunteers()
    eventi = getevents()
    riunioni = getmeetings()
    avgs = getavgcharge()
    bilanci = getfstat()
    spons = getsponsorshipdetail()

    dettagli = []
    dettaglispese = []
    for evento in eventi:
        dettaglio = getpartnersdetail(evento["titolo"])
        dettagli.append(dettaglio)

        dett = getchargesdetail(evento["titolo"])
        dettaglispese.append(dett)

    connection = connectdb()
    with connection.cursor() as cursor:
        cursor.execute("select sum(importo) as importo, evento from spese group by evento")
        rows = cursor.fetchall()
        totali = []
        for row in rows:
            totali.append({"evento": (row["evento"]), "importo": (row["importo"])})

    return render_template('home-marketing.html', volontari=volontari, eventi=eventi, riunioni=riunioni,
                           dettagli=dettagli, dettaglispese=dettaglispese, totali=totali, avgs=avgs, bilanci=bilanci,
                           spons=spons)


@app.route("/comunicazione/")
def comunicazione():
    volontari = getvolunteers()
    eventi = getevents()
    riunioni = getmeetings()
    postiliberi = []
    spettatori = getaudience()
    dettagli = []
    for evento in eventi:
        posti = {"evento": evento["titolo"], "posti": checkeventcapacity(evento["titolo"])}
        postiliberi.append(posti)

        dettaglio = getspeakerdetails(evento["titolo"])
        dettagli.append(dettaglio)

    return render_template('home-comunicazione.html', volontari=volontari, eventi=eventi, riunioni=riunioni,
                           postiliberi=postiliberi, spettatori=spettatori, dettagli=dettagli)


@app.route("/logistica/")
def logistica():
    volontari = getvolunteers()
    eventi = getevents()
    riunioni = getmeetings()
    location = getlocation()
    spettatori = getaudience()
    postiliberi = []
    dettagli = []
    for evento in eventi:
        posti = {"evento": evento["titolo"], "posti": checkeventcapacity(evento["titolo"])}
        postiliberi.append(posti)

        dettaglio = getspeakerdetails(evento["titolo"])
        dettagli.append(dettaglio)

    return render_template('home-logistica.html', volontari=volontari, eventi=eventi, riunioni=riunioni,
                           location=location, spettatori=spettatori, postiliberi=postiliberi, dettagli=dettagli)


@app.route("/regia/")
def regia():
    volontari = getvolunteers()
    eventi = getevents()
    riunioni = getmeetings()
    location = getlocation()
    durate = getduration()
    dettagli = []
    for evento in eventi:
        dettaglio = getspeakerdetails(evento["titolo"])
        dettagli.append(dettaglio)

    return render_template('home-regia.html', volontari=volontari, eventi=eventi, riunioni=riunioni, location=location,
                           dettagli=dettagli, durate=durate)


@app.route("/grafica/")
def grafica():
    volontari = getvolunteers()
    eventi = getevents()
    riunioni = getmeetings()
    spettatori = getaudience()

    postiliberi = []
    dettaglispeaker = []
    dettaglisponsor = []
    for evento in eventi:
        posti = {"evento": evento["titolo"], "posti": checkeventcapacity(evento["titolo"])}
        postiliberi.append(posti)

        dett = getspeakerdetails(evento["titolo"])
        dettaglispeaker.append(dett)

        dettaglio = getpartnersdetail(evento["titolo"])
        dettaglisponsor.append(dettaglio)

    return render_template('home-grafica.html', volontari=volontari, eventi=eventi, riunioni=riunioni,
                           dettaglispeaker=dettaglispeaker, dettaglisponsor=dettaglisponsor, spettatori=spettatori,
                           postiliberi=postiliberi)


# -------------------------------------------------------------INSERIMENTI
@app.route("/inserimento-membro-team/", methods=["POST", "GET"])
def ins_membro_team():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                cf = request.form["cf"]
                nome = request.form["nome"]
                cognome = request.form["cognome"]
                telefono = request.form["numero"]
                email = request.form["email"]
                skype = request.form["skype"]
                ruolo = request.form["ruolo"]

                sql = "INSERT INTO Volontari (CF, Nome, Cognome, Telefono, Email, Skype) VALUES(%s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (cf, nome, cognome, telefono, email, skype))

                sql = "INSERT INTO Appartenenze(Volontario, Team, Ruolo) VALUES(%s, %s, %s)"
                cursor.execute(sql, (cf, session['team'], ruolo))

                connection.commit()
                return render_template('inserimento-membro-team.html', success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-membro-team.html', error=error)

    return render_template('inserimento-membro-team.html')


@app.route("/inserimento-evento/", methods=["POST", "GET"])
def ins_evento():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                titolo = request.form["titolo"]
                tipologia = request.form["tipologia"]
                data = request.form["data"]
                licenza = request.form["licenza"]
                sito = request.form["sito"]
                location = request.form["location"]

                sql = "INSERT INTO Eventi (Titolo, Tipologia, Data, NumeroLicenza, SitoWeb, Programma) VALUES(%s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (titolo, tipologia, data, licenza, sito, 'NULL'))

                sql = "INSERT INTO Sedi(Location, Evento) VALUES(%s, %s)"
                cursor.execute(sql, (location, titolo))

                connection.commit()
                return render_template('inserimento-evento.html', success="ok")


    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-evento.html', error=error)

    return render_template('inserimento-evento.html')


@app.route("/inserimento-riunione/", methods=["POST", "GET"])
def ins_riunione():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                evento = request.form["evento"]
                data = request.form["data"]
                ora = request.form["ora"]
                luogo = request.form["luogo"]
                odg = request.form["odg"]

                sql = "INSERT INTO Riunioni (Evento, Data, Ora, Luogo, ODG) VALUES(%s, %s, %s, %s, %s)"
                cursor.execute(sql, (evento, data, ora, luogo, odg))

                connection.commit()
                return render_template('inserimento-riunione.html', success="ok")


    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-riunione.html', error=error)

    return render_template('inserimento-riunione.html')


@app.route("/inserimento-relatore/", methods=["POST","GET"])
def ins_relatore():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                nome = request.form["nome"]
                cognome = request.form["cognome"]
                cf = request.form["cf"]
                telefono = request.form["telefono"]
                email = request.form["email"]
                provenienza = request.form["provenienza"]
                professione = request.form["professione"]

                sql = "INSERT INTO Relatori (CF, Nome, Cognome, Telefono, Email, Provenienza, Professione) VALUES(%s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (cf, nome, cognome, telefono, email, provenienza, professione))

                connection.commit()
                return render_template('inserimento-relatore.html', success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-relatore.html', error=error)

    return render_template('inserimento-relatore.html')


@app.route("/inserimento-invito/<string:view>/<string:cf>", methods=["POST","GET"])
def ins_invito(view, cf):
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if view == "None":
                if request.method == "POST":
                    cf = request.form["cf"]
                    sql = "select * from spettatori where cf = (%s)"
                    cursor.execute(sql, cf)

                    if cursor.fetchone() == None:
                        view = "spettatore"
                        return render_template('inserimento-invito.html', view=view, cf=cf)

                    else:
                        view = "invito"
                        return render_template('inserimento-invito.html', view=view, cf=cf)

            elif view == "spettatore":
                if request.method == "POST":
                    nome = request.form["nome"]
                    cognome = request.form["cognome"]
                    datanascita = request.form["datanascita"]
                    sesso = request.form["sesso"]
                    residenza = request.form["residenza"]
                    occupazione = request.form["occupazione"]
                    evento = request.form["evento"]
                    codice = request.form["codice"]
                    invitante = request.form["invitatoda"]
                    rif = request.form["rif"]

                    flag = checkinvite(invitante,rif,evento)
                    posti = checkeventcapacity(evento)
                    if posti <= 0:
                        error = "L'evento e' sold-out."
                        return render_template('inserimento-invito.html', error=error, view=view)

                    if flag == "v":
                        sql = "INSERT INTO Spettatori (CF, Nome, Cognome, DataNascita, Sesso, Residenza, Occupazione) VALUES(%s, %s, %s, %s, %s, %s, %s)"
                        cursor.execute(sql, (cf, nome, cognome, datanascita, sesso, residenza, occupazione))
                        connection.commit()
                        sql = "INSERT INTO Inviti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Omaggi (Spettatore, Evento, CodiceInvito) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        sql = "INSERT INTO BenefitVolontari (Volontario, CodiceInvito, Evento) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (rif, codice, evento))
                        connection.commit()
                        return render_template('inserimento-invito.html', view=view, success="ok")


                    elif flag == "r":
                        sql = "INSERT INTO Spettatori (CF, Nome, Cognome, DataNascita, Sesso, Residenza, Occupazione) VALUES(%s, %s, %s, %s, %s, %s, %s)"
                        cursor.execute(sql, (cf, nome, cognome, datanascita, sesso, residenza, occupazione))
                        connection.commit()
                        sql = "INSERT INTO Inviti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Omaggi (Spettatore, Evento, CodiceInvito) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        sql = "INSERT INTO BenefitRelatori (Relatore, CodiceInvito, Evento) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (rif, codice, evento))
                        connection.commit()
                        return render_template('inserimento-invito.html', view=view, success="ok")

                    elif flag == "p":
                        sql = "INSERT INTO Spettatori (CF, Nome, Cognome, DataNascita, Sesso, Residenza, Occupazione) VALUES(%s, %s, %s, %s, %s, %s, %s)"
                        cursor.execute(sql, (cf, nome, cognome, datanascita, sesso, residenza, occupazione))
                        connection.commit()
                        sql = "INSERT INTO Inviti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Omaggi (Spettatore, Evento, CodiceInvito) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        sql = "INSERT INTO BenefitPartner (Partner, CodiceInvito, Evento) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (rif, codice, evento))
                        connection.commit()
                        return render_template('inserimento-invito.html', view=view, success="ok")

                    else:
                        return render_template('inserimento-invito.html', error=flag, view=view)

            elif view == "invito":
                if request.method == "POST":
                    evento = request.form["evento"]
                    codice = request.form["codice"]
                    invitante = request.form["invitatoda"]
                    rif = request.form["rif"]

                    flag = checkinvite(invitante, rif, evento)
                    posti = checkeventcapacity(evento)
                    if posti <= 0:
                        error = "L'evento e' sold-out."
                        return render_template('inserimento-invito.html', error=error, view=view)

                    if flag == "e":
                        error = "Errore nell'inserimento."
                        return render_template('inserimento-invito.html', error=error, view=view)

                    elif flag == "v":
                        sql = "INSERT INTO Inviti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Omaggi (Spettatore, Evento, CodiceInvito) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        sql = "INSERT INTO BenefitVolontari (Volontario, CodiceInvito, Evento) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (rif, codice, evento))
                        connection.commit()
                        return render_template('inserimento-invito.html', view=view, success="ok")

                    elif flag == "r":
                        sql = "INSERT INTO Inviti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Omaggi (Spettatore, Evento, CodiceInvito) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        sql = "INSERT INTO BenefitRelatori (Relatore, CodiceInvito, Evento) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (rif, codice, evento))
                        connection.commit()
                        return render_template('inserimento-invito.html', view=view, success="ok")

                    elif flag == "p":
                        sql = "INSERT INTO Inviti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Omaggi (Spettatore, Evento, CodiceInvito) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        sql = "INSERT INTO BenefitPartner (Partner, CodiceInvito, Evento) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (rif, codice, evento))
                        connection.commit()
                        return render_template('inserimento-invito.html', view=view, success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-invito.html', error=error, view=view)

    return render_template('inserimento-invito.html',view=view)


@app.route("/inserimento-biglietto/<string:view>/<string:cf>", methods=["POST","GET"])
def ins_biglietto(view, cf):
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if view == "None":
                if request.method == "POST":
                    cf = request.form["cf"]
                    sql = "select * from spettatori where cf = (%s)"
                    cursor.execute(sql, cf)

                    if cursor.fetchone() == None:
                        view = "spettatore"
                        return render_template('inserimento-biglietto.html', view=view, cf=cf)

                    else:
                        view = "biglietto"
                        return render_template('inserimento-biglietto.html', view=view, cf=cf)

            elif view == "spettatore":
                if request.method == "POST":
                    nome = request.form["nome"]
                    cognome = request.form["cognome"]
                    datanascita = request.form["datanascita"]
                    sesso = request.form["sesso"]
                    residenza = request.form["residenza"]
                    occupazione = request.form["occupazione"]
                    evento = request.form["evento"]
                    codice = request.form["codice"]

                    posti = checkeventcapacity(evento)
                    if posti <= 0:
                        error = "L'evento e' sold-out."
                        return render_template('inserimento-biglietto.html', error=error, view=view)
                    else:
                        sql = "INSERT INTO Spettatori (CF, Nome, Cognome, DataNascita, Sesso, Residenza, Occupazione) VALUES(%s, %s, %s, %s, %s, %s, %s)"
                        cursor.execute(sql, (cf, nome, cognome, datanascita, sesso, residenza, occupazione))
                        connection.commit()
                        sql = "INSERT INTO Biglietti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Acquisti (Spettatore, Evento, CodiceBiglietto) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        return render_template('inserimento-biglietto.html', view=view, success="ok")


            elif view == "biglietto":
                if request.method == "POST":
                    evento = request.form["evento"]
                    codice = request.form["codice"]

                    posti = checkeventcapacity(evento)
                    if posti <= 0:
                        error = "L'evento e' sold-out."
                        return render_template('inserimento-biglietto.html', error=error, view=view)
                    else:
                        sql = "INSERT INTO Biglietti (Evento, Codice) VALUES(%s, %s)"
                        cursor.execute(sql, (evento, codice))
                        connection.commit()
                        sql = "INSERT INTO Acquisti (Spettatore, Evento, CodiceBiglietto) VALUES(%s, %s, %s)"
                        cursor.execute(sql, (cf, evento, codice))
                        connection.commit()
                        return render_template('inserimento-biglietto.html', view=view, success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-biglietto.html', error=error, view=view)

    return render_template('inserimento-biglietto.html',view=view)


@app.route("/inserimento-intervento/", methods=["POST","GET"])
def ins_intervento():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                nome = request.form["nome"]
                cognome = request.form["cognome"]
                evento = request.form["evento"]
                titolo = request.form["titolo"]
                durata = request.form["durata"]

                sql = "SELECT CF FROM Relatori WHERE Nome=(%s) AND Cognome=(%s)"
                cursor.execute(sql, (nome, cognome) )
                cf = cursor.fetchone()

                sql = "INSERT INTO Interventi (Relatore, Evento, Titolotalk, Duratatalk) VALUES(%s, %s, %s, %s)"
                cursor.execute(sql, (cf["CF"], evento, titolo, durata))

                connection.commit()
                return render_template('inserimento-intervento.html', success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-intervento.html', error=error)

    return render_template('inserimento-intervento.html')


@app.route("/inserimento-adesione/<string:data>/<string:ora>/<string:evento>", methods=["POST","GET"])
def ins_adesione(data,ora,evento):
    volontari = getvolunteers()
    cf = ""
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                cf = request.form["cf"]

                sql = "select * from collaborazioni where volontario=(%s) and evento=(%s)"
                cursor.execute(sql, (cf, evento))
                if cursor.fetchone() != None:
                    sql = "INSERT INTO Adesioni (Evento, DataRiunione, OraRiunione, Volontario) VALUES(%s, %s, %s, %s)"
                    cursor.execute(sql, (evento, data, ora, cf))
                    connection.commit()
                    return render_template('inserimento-adesione.html', success="ok", volontari=volontari,
                                           data=data, ora=ora, evento=evento)
                else:
                    error = "Il volontario non collabora a questo evento."
                    return render_template('inserimento-adesione.html', error=error, volontari=volontari, data=data,
                                           ora=ora, evento=evento)
    except:
        if cf == "":
            return render_template('inserimento-adesione.html', volontari=volontari, data=data, ora=ora, evento=evento)
        else:
            error = "Errore nell'inserimento."
            return render_template('inserimento-adesione.html', error=error, volontari=volontari, data=data, ora=ora,
                                   evento=evento)

    return render_template('inserimento-adesione.html', volontari=volontari, data=data, ora=ora, evento=evento)


@app.route("/inserimento-collaborazione/<string:evento>", methods=["POST","GET"])
def ins_collaborazione(evento):
    volontari = getvolunteers()
    cf=""
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                cf = request.form["cf"]

                sql = "INSERT INTO Collaborazioni (Evento, Volontario) VALUES(%s, %s)"
                cursor.execute(sql, (evento, cf))
                connection.commit()
                return render_template('inserimento-collaborazione.html', success="ok", volontari=volontari,
                                       evento=evento)

    except:
        if cf == "":
            return render_template('inserimento-collaborazione.html', volontari=volontari, evento=evento)
        else:
            error = "Errore nell'inserimento."
            return render_template('inserimento-collaborazione.html', error=error, volontari=volontari, evento=evento)

    return render_template('inserimento-collaborazione.html', volontari=volontari, evento=evento)


@app.route("/inserimento-sponsorizzazione/<string:view>/<string:partner>", methods=["POST","GET"])
def ins_spons(view,partner):
    eventi = getevents()
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if view == "None":
                if request.method == "POST":
                    partner = request.form["partner"]
                    sql = "select * from partner where nome = (%s)"
                    cursor.execute(sql, partner)

                    if cursor.fetchone() == None:
                        view = "partner"
                        return render_template('inserimento-sponsorizzazione.html', view=view, partner=partner,
                                               eventi=eventi)

                    else:
                        view = "sovvenzione"
                        return render_template('inserimento-sponsorizzazione.html', view=view, partner=partner,
                                               eventi=eventi)

            elif view == "partner":
                if request.method == "POST":
                    referente = request.form["referente"]
                    email = request.form["email"]
                    telefono = request.form["telefono"]

                    evento = request.form["evento"]
                    codice = request.form["codice"]
                    importo = request.form["importo"]
                    descrizione = request.form["desc"]
                    tipo = request.form["tipo"]

                    sql = "INSERT INTO Partner (Nome, Referente, Email, Telefono) VALUES(%s, %s, %s, %s)"
                    cursor.execute(sql, (partner, referente, email, telefono))
                    sql = "INSERT INTO Sponsorizzazioni (Evento, Codice, Tipo, Importo, Descrizione) VALUES(%s, %s, %s, %s, %s)"
                    cursor.execute(sql, (evento, codice, tipo, importo, descrizione))
                    sql = "INSERT INTO Sovvenzioni (Evento, Sponsorizzazione, Partner) VALUES(%s, %s, %s)"
                    cursor.execute(sql, (evento, codice, partner))
                    cursor.execute("UPDATE Partner SET NumeroEventi = NumeroEventi + 1 WHERE Nome = %s",partner)
                    connection.commit()
                    return render_template('inserimento-sponsorizzazione.html', view=view, eventi=eventi,
                                           partner=partner, success="ok")

            elif view == "sovvenzione":
                if request.method == "POST":
                    evento = request.form["evento"]
                    codice = request.form["codice"]
                    importo = request.form["importo"]
                    descrizione = request.form["desc"]
                    tipo = request.form["tipo"]

                    sql = "INSERT INTO Sponsorizzazioni (Evento, Codice, Tipo, Importo, Descrizione) VALUES(%s, %s, %s, %s, %s)"
                    cursor.execute(sql, (evento, codice, tipo, importo, descrizione))
                    sql = "INSERT INTO Sovvenzioni (Evento, Sponsorizzazione, Partner) VALUES(%s, %s, %s)"
                    cursor.execute(sql, (evento, codice, partner))
                    cursor.execute("UPDATE Partner SET NumeroEventi = NumeroEventi + 1 WHERE Nome = %s", partner)
                    connection.commit()
                    return render_template('inserimento-sponsorizzazione.html', view=view, partner=partner,
                                           eventi=eventi, success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-sponsorizzazione.html', error=error, view=view, partner=partner,
                               eventi=eventi)

    return render_template('inserimento-sponsorizzazione.html', view=view, partner=partner, eventi=eventi)


@app.route("/inserimento-spesa/", methods=["POST","GET"])
def ins_spesa():
    eventi = getevents()
    evento=""
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:

            if request.method == "POST":
                evento = request.form["evento"]
                importo = request.form["importo"]
                codice = request.form["codice"]
                descrizione = request.form["descrizione"]

                sql = "INSERT INTO Spese (Evento, Codice, Importo, Descrizione) VALUES(%s, %s, %s, %s)"
                cursor.execute(sql, (evento, codice, importo, descrizione))

                connection.commit()
                return render_template('inserimento-spesa.html', success="ok", eventi=eventi)

    except:
        if evento=="":
            return render_template('inserimento-spesa.html', eventi=eventi)
        else:
            error = "Errore nell'inserimento."
            return render_template('inserimento-spesa.html', error=error, eventi=eventi)

    return render_template('inserimento-spesa.html', eventi=eventi)


@app.route("/inserimento-location/", methods=["POST","GET"])
def ins_location():
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                nome = request.form["nome"]
                indirizzo = request.form["indirizzo"]
                telefono = request.form["telefono"]
                capienza = request.form["capienza"]
                prezzobiglietto = request.form["prezzobiglietto"]

                sql = "INSERT INTO Location (Nome, Indirizzo, Telefono, Capienza, PrezzoBiglietto) VALUES(%s, %s, %s, %s, %s)"
                cursor.execute(sql, (nome, indirizzo, telefono, capienza, prezzobiglietto))

                connection.commit()
                return render_template('inserimento-location.html', success="ok")

    except:
        error = "Errore nell'inserimento."
        return render_template('inserimento-location.html', error=error)

    return render_template('inserimento-location.html')


# -------------------------------------------------------------ELIMINAZIONI
@app.route("/eliminazione-membro-team/", methods=["POST","GET"])
def elim_membro_team():
    volontari = getvolunteers()
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                cf = request.form["cf"]
                print(cf)
                sql = "DELETE FROM Volontari WHERE CF=(%s)"
                cursor.execute(sql, cf)
                connection.commit()
                return render_template('eliminazione-membro-team.html', success="ok", volontari=volontari)

    except:
        error = "Errore nell'eliminazione."
        return render_template('eliminazione-membro-team.html', error=error, volontari=volontari)

    return render_template('eliminazione-membro-team.html', volontari=volontari)


@app.route("/eliminazione-evento/", methods=["POST","GET"])
def elim_evento():
    eventi = getevents()
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                titolo = request.form["titolo"]
                sql = "DELETE FROM Eventi WHERE Titolo=(%s)"
                cursor.execute(sql, titolo)
                connection.commit()
                return render_template('eliminazione-evento.html', success="ok", eventi=eventi)

    except:
        error = "Errore nell'eliminazione."
        return render_template('eliminazione-evento.html', error=error, eventi=eventi)

    return render_template('eliminazione-evento.html', eventi=eventi)


@app.route("/eliminazione-riunione/", methods=["POST","GET"])
def elim_riunione():
    riunioni = getmeetings()
    id = ""
    try:
        # connessione al database
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                id = request.form["id"]
                for riunione in riunioni:
                    if riunione["ID"] == id:
                        rc = riunione
                sql = "DELETE FROM Riunioni WHERE Evento=(%s) AND Data=(%s) AND Ora=(%s)"
                cursor.execute(sql, (rc["evento"], rc["data"], rc["ora"]))
                connection.commit()
                return render_template('eliminazione-riunione.html', success="ok", riunioni=riunioni)

    except:
        if id == "":
            return render_template('eliminazione-riunione.html', riunioni=riunioni)
        else:
            error = "Errore nell'eliminazione."
            return render_template('eliminazione-riunione.html', error=error, riunioni=riunioni)

    return render_template('eliminazione-riunione.html', riunioni=riunioni)


@app.route("/eliminazione-relatore/", methods=["POST","GET"])
def elim_relatore():
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                cf = request.form["cf"]
                sql = "DELETE FROM Relatore WHERE CF=(%s)"
                cursor.execute(sql, cf)
                connection.commit()
                return render_template('eliminazione-relatore.html', success="ok")

    except:
        error = "Errore nell'eliminazione."
        return render_template('eliminazione-relatore.html', error=error)

    return render_template('eliminazione-relatore.html')


@app.route("/eliminazione-spesa/", methods=["POST","GET"])
def elim_spesa():
    eventi = getevents()
    spese = getcharges()
    codice=""
    try:
        connection = connectdb()

        with connection.cursor() as cursor:
            if request.method == "POST":
                evento = request.form["evento"]
                codice = request.form["spesa"]
                sql = "DELETE FROM spese WHERE codice=(%s) and evento=(%s)"
                cursor.execute(sql, (codice, evento))
                connection.commit()
                return render_template('eliminazione-spesa.html', success="ok", eventi=eventi, spese=spese)

    except:
        if codice=="":
            return render_template('eliminazione-spesa.html', eventi=eventi, spese=spese)
        else:
            error = "Errore nell'eliminazione."
            return render_template('eliminazione-spesa.html', error=error, eventi=eventi, spese=spese)

    return render_template('eliminazione-spesa.html',eventi=eventi, spese=spese)




# -------------------------------------------------------------DETTAGLI
@app.route("/dettaglio-volontario/<string:cf>", methods=["POST","GET"])
def dett_volontario(cf):
    connection = connectdb()
    with connection.cursor() as cursor:

        cursor.execute(
            "select v.*, a.ruolo as ruolo from volontari v, appartenenze a where a.volontario = v.cf and v.cf = (%s)",
            cf)
        v = cursor.fetchone()
        volontario = {"cf": (v["CF"]), "nome": (v["Nome"]), "cognome": (v["Cognome"]), "telefono": (v["Telefono"]),
                      "skype": (v["Skype"]), "email": (v["Email"]), "ruolo": (v["ruolo"])}

        cursor.execute("select evento from collaborazioni where volontario = (%s)", cf)
        rows = cursor.fetchall()
        collaborazioni = []
        for row in rows:
            collaborazioni.append(row["evento"])

        cursor.execute(
            "select b.codiceinvito, b.evento, s.nome, s.cognome from benefitvolontari b, omaggi o, spettatori s where b.volontario = (%s) and o.codiceinvito = b.codiceinvito and s.cf = o.spettatore",
            cf)
        rows = cursor.fetchall()
        inviti = []
        for row in rows:
            inviti.append({"evento": row["evento"], "codice": row["codiceinvito"], "nome": row["nome"],
                           "cognome": row["cognome"]})

    return render_template('dettaglio-volontario.html', volontario=volontario, collaborazioni=collaborazioni,
                           inviti=inviti)


@app.route("/dettaglio-evento/<string:titolo>", methods=["POST","GET"])
def dett_evento(titolo):
    connection = connectdb()

    with connection.cursor() as cursor:
        cursor.execute(
            "select v.nome, v.cognome, a.team from collaborazioni c, volontari v, appartenenze a where v.cf = c.volontario and a.volontario = v.cf and evento=(%s)",
            titolo)
        rows = cursor.fetchall()
        volontari = []
        for row in rows:
            volontario = {"nome": (row["nome"]), "cognome": (row["cognome"]), "team": (row["team"])}
            volontari.append(volontario)

        cursor.execute(
            "select e.*, s.location as location from eventi e, sedi s where s.evento=e.titolo and e.titolo=(%s)",
            titolo)
        row = cursor.fetchone()
        evento = {"titolo": (row["Titolo"]), "tipologia": (row["Tipologia"]), "data": (row["Data"]),
                  "licenza": (row["NumeroLicenza"]), "sito": (row["SitoWeb"]), "programma": (row["Programma"]),
                  "location": (row["location"])}

    return render_template('dettaglio-evento.html', evento=evento, volontari=volontari)


@app.route("/dettaglio-riunione/<string:data>/<string:ora>/<string:evento>", methods=["POST","GET"])
def dett_riunione(data, ora, evento):
    connection = connectdb()

    with connection.cursor() as cursor:
        cursor.execute("select * from riunioni where data = (%s) and ora = (%s) and evento = (%s)", (data, ora, evento))
        row = cursor.fetchone()
        riunione = {"evento": (row["Evento"]), "data": (row["Data"]), "ora": (row["Ora"]),
                    "luogo": (row["Luogo"]), "odg": (row["ODG"])}

        cursor.execute(
            "select v.nome, v.cognome, ap.team as team from adesioni a, volontari v, appartenenze ap where a.volontario = v.cf and ap.volontario = v.cf and a.datariunione = (%s) and a.orariunione = (%s) and a.evento = (%s)",
            (data, ora, evento))
        rows = cursor.fetchall()
        adesioni = []
        for row in rows:
            adesione = {"nome": row['nome'], "cognome": row['cognome'], "team": row["team"]}
            adesioni.append(adesione)

    return render_template('dettaglio-riunione.html', riunione=riunione, adesioni=adesioni)


@app.route("/dettaglio-partner/<string:partner>", methods=["POST","GET"])
def dett_partner(partner):
    connection = connectdb()
    with connection.cursor() as cursor:
        cursor.execute("select * from partner where nome = (%s)", partner)
        row = cursor.fetchone()
        p = {"nome": (row["Nome"]), "referente": (row["Referente"]), "email": (row["Email"]),
             "telefono": (row["Telefono"]), "categoria": (row["Categoria"])}

        cursor.execute(
            "select sv.evento, sp.codice, sp.tipo, sp.importo, sp.descrizione  from sovvenzioni sv, sponsorizzazioni sp where sv.sponsorizzazione = sp.codice and sv.partner = (%s)",
            partner)
        rows = cursor.fetchall()
        sponsorizzazioni = []
        for row in rows:
            spons = {"evento": row['evento'], "codice": row['codice'], "tipo": row["tipo"], "importo": row["importo"],
                     "descrizione": row["descrizione"]}
            sponsorizzazioni.append(spons)

    return render_template('dettaglio-partner.html', partner=p, sponsorizzazioni=sponsorizzazioni)


@app.route("/dettaglio-location/<string:location>", methods=["POST","GET"])
def dett_location(location):
    eventi = getevents()
    connection = connectdb()
    with connection.cursor() as cursor:
        cursor.execute("select * from location where nome=(%s)", location)
        row = cursor.fetchone()
        location = {"nome": (row["Nome"]), "indirizzo": (row["Indirizzo"]), "telefono": (row["Telefono"]),
                    "capienza": (row["Capienza"]), "prezzobiglietto": (row["PrezzoBiglietto"])}

    return render_template('dettaglio-location.html', location=location, eventi=eventi)


@app.route("/dettaglio-relatore/<string:cf>", methods=["POST","GET"])
def dett_relatore(cf):
    connection = connectdb()
    with connection.cursor() as cursor:
        cursor.execute("select * from relatori where cf=(%s)", cf)
        row = cursor.fetchone()
        relatore = {"nome": (row["Nome"]), "cognome": (row["Cognome"]), "cf": (row["CF"]),
                    "professione": (row["Professione"]), "provenienza": (row["Provenienza"]), "email": (row["Email"]),
                    "telefono": (row["Telefono"])}

        cursor.execute("select * from interventi where relatore=(%s)", cf)
        rows = cursor.fetchall()
        interventi=[]
        for row in rows:
            interventi.append({"evento": (row["Evento"]), "titolotalk": (row["TitoloTalk"])})

    return render_template('dettaglio-relatore.html', relatore=relatore, interventi=interventi)


if __name__ == "__main__":
    app.run()
